#!/bin/sh

set -e

DESTDIR=$1

if [[ -z "$DESTDIR" ]]; then
    echo "error: missing destination directory"
    exit 1
fi

if [[ -d $DESTDIR ]]; then
    echo "error: destination directory already exists"
    exit 1
fi

# Step 1.
mkdir $DESTDIR

cd $DESTDIR

# Step 2.
cat > hiera.yaml <<EOF
version: 5
defaults:
  datadir: hieradata
  data_hash: yaml_data

hierarchy:
  - name: "Common data"
    path: "common.yaml"
EOF

# Step 3. Create test.pp
cat > test.pp <<EOF
notify { "This is a local Puppet repo.": }
EOF

# Step 4.
mkdir hieradata
echo '---' > hieradata/common.yaml

# Step 5. Create a local puppet-apply command
CURDIR=`pwd`
cat > pa.sh <<EOF
#!/bin/bash
# Run puppet locally
puppet apply --modulepath ${CURDIR} --hiera_config ${CURDIR}/hiera.yaml \$@
EOF
chmod a+x pa.sh

# Step 6. Create the README file
CURDIR=`pwd`
cat > README <<EOF
To install a Puppet forge module:
  puppet module install --modulepath=${CURDIR} --target-dir=${CURDIR} puppetlabs/stdlib

To run a Puppet apply:
  puppet apply --modulepath ${CURDIR} --hiera_config ${CURDIR}/hiera.yaml test.pp
EOF
