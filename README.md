# Testing your Puppet code locally

## Quick start

1. Download [setup-puppet-local.sh](../setup-puppet-local.sh):

        $ wget https://code.stanford.edu/puppetpublic/puppet-local-testing/raw/master/setup-puppet-local.sh
        $ chmod a+x setup-puppet-local.sh

1. Run the script. The script will create a new directory in the current directory
with your local Puppet repo:

        $ ./setup-puppet-local.sh local
        # If you don't like 'local' as a directory use something else

1. Change into the new directory and `cat` the README file for instructions:

        $ cd local
        $ cat README

1. Do your Puppet development. To run Puppet apply locally use the command
described in the `README` file, or else use the provided `pa.sh` script:

        $ edit test.pp (make some changes)
        $ ./pa.sh test.pp

## Details

The following describes what the above script does

1. Start with a base directory, say `/tmp/puppet`.

1. Create the hiera.yaml file in `/tmp/puppet/hiera.yaml`:

        # /tmp/puppet/hiera.yaml
        version: 5
        defaults:
          datadir: hieradata
          data_hash: yaml_data

        hierarchy:
          - name: "Common data"
            path: "common.yaml"

1. Create the hieradata directory and add common.yaml:

        mkdir /tmp/puppet/hieradata
        echo '---' > /tmp/puppet/hieradata/common.yaml

1. Put your classes in `/tmp/puppet`. For example:

        /tmp/puppet/kdc
        /tmp/puppet/kdc/manifests
        /tmp/puppet/kdc/manifests/init.pp

1. Put your test code that calls the other classes in `/tmp/puppet/test.pp`:

        # /tmp/puppet/test.pp
        include kdc

1. To install a PuppetForge module:

        puppet module install --modulepath=/tmp/puppet --target-dir=/tmp/puppet puppetlabs/stdlib

1. Run this command to test:

        puppet apply --modulepath /tmp/puppet --hiera_config /tmp/puppet/hiera.yaml test.pp
